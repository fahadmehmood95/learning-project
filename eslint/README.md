# ESLint Presentations & Video 

Introduction to ESLint
- https://www.youtube.com/watch?v=PFn2xSAunr8
- https://www.youtube.com/watch?v=_B4KiclD2X4 

ES Lint Demo 
- https://youtu.be/7h2eLw3CJjo

# Setup (React Project)

- Clone project
- Goto folder
- Install dependencies `npm install`
- To install eslint in project, run command `npm install eslint --save-dev`.
- To setup eslint in project, run command `./node_modules//.bin/eslint --init`. Select the appropriate answers to questions related to your project configurations (select React as framework). Select airbnb javascript style guide to follow the standard.
- Add following scripts in `package.json`
  - "start-lint": "npm run lint && react-scripts start" -- to apply linting first and then run project
  - "lint": "eslint src/**/*.js" -- apply linting on all js files in src
  - "lint-fix": "eslint src/**/*.js --fix" -- to fix errors/warnings
- Add file `.eslintrc.f3.js` to root folder. This is a standard javascript rules file. Initially, all rules are set to warn.
- Add `./.eslintrc.f3.js` in `extends` in eslint config file.
- Run command `npm run start-lint`
- Run command `npm run lint-fix` to fix errors/warnings that can be fixed automatically. Remaining will have to be fixed manually
- `Eslint` vs code extension can be installed and set as code formatter for workspace to see errors/warnings in Problems tab on VS code



# Setup (Sample Project)

- Create a new folder
- Run command `npm init`
- To install eslint in project, run command `npm install eslint --save-dev`.
- To setup eslint in project, run command `./node_modules//.bin/eslint --init`. Select the appropriate answers to questions related to your project configurations. Select airbnb javascript style guide to follow the standard.
- Create a new file `index.js` at root folder
- Write some sample code in `index.js` to test eslint
- Add file `.eslintrc.f3.js` to root folder. This is a standard rules file. Initially, all rules are set to warn.
- Add `./.eslintrc.f3.js` in `extends` in eslint config file.
- Add script `"lint": "eslint"` in `package.json`
- Run command `npm run lint index.js` to run eslint airbnb rules on index.js
- `Eslint` vs code extension can be installed and set as code formatter for workspace to see errors/warnings in Problems tab on VS code
- Add script `"lint-fix": "eslint --fix"` in package.json
- Run command `npm run lint-fix index.js` to fix errors/warnings that can be fixed automatically. Remaining will have to be fixed manually
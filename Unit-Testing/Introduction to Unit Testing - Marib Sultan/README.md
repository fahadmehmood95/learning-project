# Unit Testing Overview

### Presentation & Video

- Video  : https://youtu.be/Mt4vKeyytJQ
- Presentation    : https://bit.ly/3jeKBMa


### What

Process of testing individual units of a software, these units may include 

- Functions, Statements, Loops, Classes etc


### Why

- Identification of bugs becomes easier and quicker
- Expected results can be tested immediately
- Reduces cost of change / identification of unwanted change
- Confident refactoring
- Testing is essential for CI/CD pipelines
- Lays the foundation for the testing pyramid



### Ecosystem

- Test launchers
- Testing structure providers
- Assertions functions
- Progress/Summary generator
- Mocks/Spies
- Snapshots
- Coverage reports
- Browser controllers



### Demo usage

Install the dependencies using:
> npm install

and then run the following to start tests
> npm run test







# Unit Testing Setup for React
  A basic steps for the react application to integrate with initial unit testing setup using chai, mocha, sinon and enzyme

## Prerequisite
- React Setup already up and running

## Dependencies
- Install chai and mocha, `npm install chai mocha --save-dev`
- Install karma along with its plugins, here we are installing required plugins as per our desired libraries (chai, mocha, sinon, and enzyme)
run command `npm install karma karma-chai karma-mocha karma-chrome-launcher karma-coverage karma-spec-reporter karma-webpack --save-dev`.
  - `karma-chrome-launcher` is to support the chrome browser, if you are willing to include other browsers then you must include the karma plugin for that browser.
  - `karma webpack` is neccecary when you are working with ES6, it allows to use webpack to pre-process files in karma.
- Install sinon and enzyme `npm install sinon enzyme enzyme-adapter-react-16 --save-dev`
  - `enzyme-adapter` it helps to adapt based on react version, in this case react '16' was used. So do install appropriate adapter based on the react version you are using, for exapmple `enzyme-adapter-react-17` should be used with react 17.

## Configurations
  - Next step is to build the karma configuration file, this can be done using [karma init](http://karma-runner.github.io/6.3/intro/configuration.html) or `karma init <filename>`, 

![Karma-Init](/Unit-Testing/images/karma-init.png?raw=true "Karma Init")

  - If you choose to name the file, then don't forget to add it in the package.json scritps, by default it will pick "karma.conf.js"

![Karma-Script-Change](/Unit-Testing/images/karma-script.png?raw=true "Karma-Script-Change")

  - Add missing pieces in karma init file 
      - Plugins should be added, in our case it would be:


    ```javascript
        plugins: [
      'karma-webpack',
      'karma-mocha',
      'karma-chai',
      'karma-chrome-launcher',
      'karma-coverage',
      'karma-spec-reporter'
      ], 
    ```
    
      - All the required frame works should be added i;e `mocha, chai and webpack`, (while generating the file it only added mocha)


    ```javascript
      // frameworks to use
      // available frameworks: https://www.npmjs.com/search?q=keywords:karma-adapter
      frameworks: ['mocha', 'chai', 'webpack'],
    ```

      - Add the `basePath` and `files`.
        
    ```javascript
     // base path that will be used to resolve all patterns (eg. files, exclude)
      basePath: 'src',

      // list of files / patterns to load in the browser
      files: [
        'tests/*.js',
      ],
    ```
    




      ###### BasePath to Test folder in our case
      ![Karma-Basepath](/Unit-Testing/images/basPath.png?raw=true "Karma-Basepath")






    - Do add the preprocessor if there is any, we are using `webpack`     

    ```javascript
      // preprocess matching files before serving them to the browser
      // available preprocessors: https://www.npmjs.com/search?q=keywords:karma-preprocessor
      preprocessors: {
        'tests/*.js': ['webpack']
      },
    ```

    - (Optional) We also added a reporting features, which will help to determine the code coverage and specifications

    ```javascript
      // test results reporter to use
      // possible values: 'dots', 'progress'
      // available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
      reporters: ['spec', 'coverage'],
    ```

    A sample karma config file can be found [here](./Sample test code - Imran Alam/karma.conf.js)

## Test File:
  - Now all the external configurations are done and we are ready to write the tests.
  - Specifially for enzyme the adapter needs to be configured in the test file
  ```javascript
  import Adapter from 'enzyme-adapter-react-16';
  enzyme.configure({ adapter: new Adapter() });
  ```
  - A sample code which might help in using some of the basic syntax:

  ```javascript
  import React from 'react';
  import enzyme, { mount } from 'enzyme';
  import Adapter from 'enzyme-adapter-react-16';
  import sinon from 'sinon';
  import ButtonPanel from "../component/ButtonPanel";

  enzyme.configure({ adapter: new Adapter() });

  /* Moacha Syntax
     Test Suite, a multiple tests can be written under this
     for example, if a functionality has many test cases, all of them can lie under one 'describe'
  */

  describe('button panel should display and work correctly', function() {
    let eventHandler;
    let component;

    
    /* Moacha Syntax
       This hook executes once for each test unit before that test unit is executed. It is used to initialize unit related resources.
    */
    beforeEach(function(){
      eventHandler = sinon.spy();
      component = mount(<ButtonPanel clickHandler={eventHandler} />);
    });

    
    /* Moacha Syntax
       This hook executes once for each test unit after that test unit is executed. It is used to release unit related resources.
    */
    afterEach(function(){
      component.unmount();
    });


    /* Moacha Syntax
       A single test within a test suite
    */
    it('should render component', function() {
      const myComponent = component.find('[name="0"]');

    /* Chai Syntax
       expect: One of the BDD styles that chai provide, a chainable object
       equal: Asserts that the target is strictly (===) equal to the given val.
    */
      expect(myComponent.length).to.equal(1);
    });


    it('should send numbers if number button is clicked', function() {
      const component2 = component.find('[name="2"] button');
      expect(button2.length).to.equal(1);
        
      /* Enzyme Syntax
       simulate: a method to trigger an event with the help of component
      */
      component2.simulate('event');
      expect(eventHandler.calledOnce).to.be.true;
      expect(eventHandler.calledWith("2")).to.be.true;
    });
  });
  ```

  - Run the tests using `karma start` or ideally `npm run test` script command, if it doesn't work then check if the scripts in package.json are configured correctly or not
    ```javascript
    "scripts": {
    "test": "karma start",
    ```
  - Do read the official documentations when writing the complex level tests.
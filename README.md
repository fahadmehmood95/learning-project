

# JS Engineering Team


### [HTML5 APIs](https://bitbucket.org/folio3/js/src/master/HTML5%20APIs/)

Modern HTML5 Web APIs descriptions and usage.

### [Unit Testing](https://bitbucket.org/folio3/js/src/master/Unit-Testing/)

1. What is Unit testing ?
1. Why we use it ?
1. Unit testing echo system
1. List of tools and thier purpose.

### [Webpack](https://bitbucket.org/folio3/js/src/master/WebPack/)

Introduction to webpack with a simple example.

### [ESLint](https://bitbucket.org/folio3/js/src/master/eslint/)

1. What is Eslint ?
1. Integration and Configuration

### [Prettier](https://bitbucket.org/folio3/js/src/master/perttier%20%2B%20eslint/)

1. What is Prettier ?
1. How to use Prettier ?
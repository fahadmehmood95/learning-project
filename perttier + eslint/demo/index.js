// Function examples
const tooLong = (argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7) => {
  console.log(argument0, argument1, argument2, argument3, argument4, argument5, argument6, argument7);
};

const justRight = (argument0, argument1, argument2, argument3) => {
  console.log(argument0, argument1, argument2, argument3);
};

tooLong();
justRight();

// Object and list example
const thisIsAnExampleObject = {
  item_a: "item_a",
  item_b: "item_b",
  item_c: "item_c",
  item_d: "item_d",
  item_e: ["list_item_1", "list_item_2"],
};

console.log(thisIsAnExampleObject);

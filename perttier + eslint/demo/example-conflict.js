/*
  This example breaks two rules:
  1. "array-bracket-spacing" (line 8)
  2. "no-trailing-spaces" (line 10)
*/

function exampleConflict() {
  const myArr = [1, 2, 3, 4, 5];

  return myArr; 
}

exampleConflict();

# Prettier Presentations & Video 

Introdcution to Prettier + Using Prettier with ESLint

https://youtu.be/L0GEa1IoVuI

## Installation & Usage

### INSTALLING PRETTIER (STANDALONE)

- Open your project, and in the terminal:
  >  npm install --save-dev --save-exact prettier

- Create a config file named `.prettierrc`

- Create an ignore file `.prettierignore`


### USING PRETTIER FROM COMMANDLINE:

- Print formatted code on the terminal for a file:
  > node_modules/.bin/prettier index.js

- Print formatted code for all files in a specific directory:
  > node_modules/.bin/prettier myfolder/

- Print for all files inside the current directory:
  > node_modules/.bin/prettier .

- Format all files:
  > node_modules/.bin/prettier --write .

- Format all files in specific directory:
  > node_modules/.bin/prettier --write myfolder/

- Format a specific file:
  > node_modules/.bin/prettier --write myfolder/myapp.js


### PRETTIER IN VSCODE:

- Install the Prettier extension from the VS Code Marketplace

- Two configuration options:
  - Configure from VS Code settings
  - Or use `.prettierrc` file (this overrides options in the VS Code settings)

- Format your code by selecting and pressing `Ctrl+K` then `Ctrl+F`


### ESLINT INTEGRATION:

- Configure ESLint in your project

- Install Prettier npm package as guided above

- Install the Prettier plugin for ESLint
  > npm install --save-dev eslint-plugin-prettier 

- Also install this to ensure that ESLint rules don’t clash with Prettier
  > npm install –save-dev eslint-config-prettier

- Update your .eslintrc file and add configuration for prettier:
  - Add `"plugin:prettier/recommended"`, and `"prettier"` in `"extends"`
  - E.g.:
  ```
  {
    ...
    "extends": ["airbnb-base", "plugin:prettier/recommended", "prettier"],
    ...
  }
  ```


### USEFUL LINKS:
- https://prettier.io/docs/en/index.html
 
- https://github.com/prettier/eslint-config-prettier
 
- https://github.com/prettier/eslint-plugin-prettier
 
- https://prettier.io/docs/en/option-philosophy.html
 
- https://prettier.io/docs/en/configuration.html
